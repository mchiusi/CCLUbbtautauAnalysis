import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import mplhep

plt.style.use(mplhep.style.CMS)

def deltaR(eta1, phi1, eta2, phi2):
    dEta = eta1 - eta2
    dPhi = np.abs(phi1 - phi2)
    dPhi = np.where(dPhi > np.pi, 2 * np.pi - dPhi, dPhi)  # Adjust phi difference
    return np.sqrt(dEta**2 + dPhi**2)

def select_tau_plot(df):
    sel_taus_eta, sel_taus_phi, sel_taus_pT, sel_taus_mass = [], [], [], []
    genTau_eta, genTau_phi, genTau_pT, genTau_mass = [], [], [], []
    for row in df.itertuples():
        for boosted_tau in range(row.nboostedTau):
            for genVisTau in range(row.nGenVisTau):
                dR = deltaR(row.boostedTau_eta[boosted_tau], row.boostedTau_phi[boosted_tau], 
                            row.GenVisTau_eta[genVisTau], row.GenVisTau_phi[genVisTau])
                if dR < 0.5:
                    sel_taus_eta.append(row.boostedTau_eta[boosted_tau])
                    sel_taus_phi.append(row.boostedTau_phi[boosted_tau])
                    sel_taus_pT.append(row.boostedTau_pt[boosted_tau])
                    sel_taus_mass.append(row.boostedTau_mass[boosted_tau])
                    genTau_eta.append(row.GenVisTau_eta[genVisTau])
                    genTau_phi.append(row.GenVisTau_phi[genVisTau])
                    genTau_pT.append(row.GenVisTau_pt[genVisTau])
                    genTau_mass.append(row.GenVisTau_mass[genVisTau])
                    # # Remove the matched gen_tau from the collection
                    list(row.GenVisTau_eta).pop(genVisTau)
                    list(row.GenVisTau_phi).pop(genVisTau)
                    break
    return sel_taus_eta, sel_taus_phi, sel_taus_pT, sel_taus_mass, genTau_eta, genTau_phi, genTau_pT, genTau_mass

def plot_histogram(boosted, gen, gen_baseline, var, bins=15):
    bin_edges = np.histogram_bin_edges(np.concatenate((boosted, gen, gen_baseline)), bins=bins)
    plt.hist(boosted, bins=bin_edges, alpha=0.6, label='boostedTau boosted', density=True)
    plt.hist(gen, bins=bin_edges, alpha=0.6, label='genTau boosted', density=True)
    plt.hist(gen_baseline, bins=bin_edges, alpha=0.6, label='genTau baseline', density=True)
    plt.xlabel(var) 
    mplhep.cms.label('Preliminary', data=True, rlabel='boostedTau ' + var)
    plt.legend()
    plt.grid()
    plt.tight_layout()
    plt.savefig('test/plots/boostedTau_'+var+'.pdf')
    plt.savefig('test/plots/boostedTau_'+var+'.png')
    plt.clf()

def plot_histos(df_boosted, df_baseline):
    # among boosted taus, choose the 2 corresponfing to genTau
    sel_taus_eta, sel_taus_phi, sel_taus_pT, sel_taus_mass, \
    genTau_eta, genTau_phi, genTau_pT, genTau_mass = select_tau_plot(df_boosted)
    _, _, _, _, genTau_eta_base, genTau_phi_base, genTau_pT_base, genTau_mass_base = select_tau_plot(df_baseline)

    # histogram for boostedTau_eta
    plot_histogram(sel_taus_pT, genTau_pT, genTau_pT_base, 'pT')
    plot_histogram(sel_taus_phi, genTau_phi, genTau_phi_base, 'phi')
    plot_histogram(sel_taus_eta, genTau_eta, genTau_eta_base, 'eta')
    plot_histogram(sel_taus_mass, genTau_mass, genTau_mass_base, 'mass')
    plot_histogram([abs(sel_taus_eta[i] - sel_taus_eta[i + 1]) for i in range(0, len(sel_taus_eta)-1, 2)],
                   [abs(genTau_eta[i] - genTau_eta[i + 1]) for i in range(0, len(genTau_eta)-1, 2)],
                   [abs(genTau_eta_base[i] - genTau_eta_base[i + 1]) for i in range(0, len(genTau_eta_base)-1, 2)],
                   'DeltaEta')