import argparse
import uproot
import pandas as pd
import tools_boosted_taus as tools

bJet_pt = 15
bJet_eta = 2.7
genTau_pt = 20
genTau_eta = 2.4

def check_gen_bJets(row):
    flavours = row["GenJet_hadronFlavour"]
    pts = row["GenJet_pt"]
    etas = row["GenJet_eta"]
    
    indices_b_jets = [i for i, flavour in enumerate(flavours) if flavour == 5]
    # has 2 bJets?
    if len(indices_b_jets) < 2:
        return False
    
    # check the corresponding pt and eta values
    valid_count = sum(
        1 for i in indices_b_jets
        if pts[i] > bJet_pt and abs(etas[i]) < bJet_eta
    )
    return valid_count >= 2

def check_genvistau(row):
    nGenVisTau = row["nGenVisTau"]
    pts  = row["GenVisTau_pt"]
    etas = row["GenVisTau_eta"]
    
    # check if there are at least two GenVisTau
    # we are looking at the tautau channel only !!!!
    assert nGenVisTau == len(pts)
    if len(pts) < 2:
        return False
    
    # check the corresponding pt and eta values
    valid_count = sum(
        1 for i in range(len(pts))
        if pts[i] > genTau_pt and abs(etas[i]) < genTau_eta
    )
    return valid_count >= 2

def check_resolved_taus(row):
    nGenVisTau = row["nGenVisTau"]
    phis = row["GenVisTau_phi"]
    etas = row["GenVisTau_eta"]
    if row["nTau"] < 2 or row["nGenVisTau"] < 2:
        return False

    # Check matching to the genVisTau
    matched_indices = []
    for tau in range(row["nTau"]):
        for gen_tau in range(nGenVisTau):
            dR = tools.deltaR(row["Tau_eta"][tau], row["Tau_phi"][tau], 
                              etas[gen_tau], phis[gen_tau])
            if dR < 0.5:
                matched_indices.append(tau)
                # Remove the matched gen_tau from the collection
                etas.pop(gen_tau)
                phis.pop(gen_tau)
                nGenVisTau -= 1
                break
    
    if len(matched_indices) < 2:
        return False
    
    # Check if the selected taus are separated by deltaR > 0.5
    sorted_indices = sorted(matched_indices, key=lambda idx: row["Tau_pt"][idx], reverse=True)
    tau1_idx, tau2_idx = sorted_indices[:2]
    dR_tau1_tau2 = tools.deltaR(
        row["Tau_eta"][tau1_idx], row["Tau_phi"][tau1_idx],
        row["Tau_eta"][tau2_idx], row["Tau_phi"][tau2_idx]
    )
    return dR_tau1_tau2 > 0.5

def check_boosted_taus(row):
    nGenVisTau = row["nGenVisTau"]
    phis = row["GenVisTau_phi"]
    etas = row["GenVisTau_eta"]
    if row["nboostedTau"] < 2 or row["nGenVisTau"] < 2:
        return False

    # Check matching to genVisTau
    # different boosted taus can match with the same genVisTau..
    matched_counts = 0
    for boosted_tau in range(row["nboostedTau"]):
        for genVisTau in range(nGenVisTau):
            dR = tools.deltaR(row["boostedTau_eta"][boosted_tau], row["boostedTau_phi"][boosted_tau], 
                              etas[genVisTau], phis[genVisTau])
            if dR < 0.5:
                matched_counts += 1
                # Remove the matched gen_tau from the collection
                etas.pop(genVisTau)
                phis.pop(genVisTau)
                nGenVisTau -= 1
                break
    return matched_counts >= 2

def process_file(file_path):
    branches = ["event", "GenJet_hadronFlavour", "GenJet_pt", "GenJet_eta",
                "nGenVisTau", "GenVisTau_pt", "GenVisTau_eta", "GenVisTau_phi",
                "GenVisTau_mass", "nTau", "Tau_pt", "Tau_eta", "Tau_phi", "nboostedTau", 
                "boostedTau_pt", "boostedTau_eta", "boostedTau_phi", "boostedTau_mass"]
    
    with uproot.open('root://xrootd-cms.infn.it/' + file_path) as f:
        tree = f["Events"]
        df = tree.arrays(branches, library="pd")
        print("File containing", df.shape[0], "events.")
        # df= df.head()

        baseline_df = df[df.apply(check_gen_bJets, axis=1) &
                         df.apply(check_genvistau, axis=1)]
        
        resTaus_df = df[df.apply(check_resolved_taus, axis=1)]
        boostedTaus_df = df[df.apply(check_boosted_taus, axis=1)]

        # insersection: baseline && boostedTaus_df && !resTaus_df
        baseline_events = set(baseline_df['event'])
        boostedTaus_events = set(boostedTaus_df['event'])
        resTaus_events = set(resTaus_df['event'])

        # Compute the set of events that satisfy baseline && boostedTaus && (!resTaus)
        boostedTaus = (baseline_events & boostedTaus_events) - resTaus_events
        return df[df['event'].isin(list(boostedTaus))], baseline_df


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Skimm signal ROOT files to evaluate nb boosted taus.")
    parser.add_argument('--file_list', type=str, help="Path to the text file containing the list of ROOT files.",
                        default='/store/mc/Run3Summer22NanoAODv12/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_LHEweights_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/2550000/bcf455c7-ec61-4f02-95e6-74fa42f814fb.root')
    args = parser.parse_args()
    
    # Read the list of file from the text file
    with open(args.file_list, 'r') as f:
        file_paths = [line.strip() for line in f if line.strip()]
        
    boostedTaus_df, baseline_df = pd.DataFrame(), pd.DataFrame()
    for file_path in file_paths:
        print(f"Processing file: {file_path}")
        boostedTaus_file, baseline_file = process_file(file_path)
        boostedTaus_df = pd.concat([boostedTaus_df, boostedTaus_file], ignore_index=True)
        baseline_df    = pd.concat([baseline_df, baseline_file], ignore_index=True)

    tools.plot_histos(boostedTaus_df, baseline_df)
    ratio = len(boostedTaus_df) / len(baseline_df)
    print(f"Ratio: {ratio}, Number of Boosted Taus: {len(boostedTaus_df)}")      