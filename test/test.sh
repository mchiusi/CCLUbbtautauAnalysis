echo "##############################################################################################################################################################################"
echo "MC 2022 preEE"
echo ""

law run PreprocessRDF --version picoTest --config-name run3_2022_preEE --category-name base --modules-file run3_2022_modulesrdf --keep-and-drop-file run3_keep_and_drop_file  --workers 50 --branch 0 --dataset-name GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00

echo ""
echo "##############################################################################################################################################################################"
echo "DATA 2022 preEE"
echo ""

law run PreprocessRDF --version picoTest --config-name run3_2022_preEE --category-name base --modules-file run3_2022_modulesrdf --keep-and-drop-file run3_keep_and_drop_file  --workers 50 --branch 0 --dataset-name Tau_2022D

echo ""
echo "##############################################################################################################################################################################"
echo "MC 2022 postEE"
echo ""

law run PreprocessRDF --version picoTest --config-name run3_2022_postEE --category-name base --modules-file run3_2022_modulesrdf --keep-and-drop-file run3_keep_and_drop_file  --workers 50 --branch 0 --dataset-name GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00

echo ""
echo "##############################################################################################################################################################################"
echo "DATA 2022 postEE"
echo ""

law run PreprocessRDF --version picoTest --config-name run3_2022_postEE --category-name base --modules-file run3_2022_modulesrdf --keep-and-drop-file run3_keep_and_drop_file  --workers 50 --branch 0 --dataset-name Tau_2022G

echo ""
echo "##############################################################################################################################################################################"
echo "MC 2023 preBPix"
echo ""

law run PreprocessRDF --version picoTest --config-name run3_2023_preBPix --category-name base --modules-file run3_2023_modulesrdf --keep-and-drop-file run3_keep_and_drop_file  --workers 50 --branch 0 --dataset-name GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00

echo ""
echo "##############################################################################################################################################################################"
echo "DATA 2023 preBPix"
echo ""

law run PreprocessRDF --version picoTest --config-name run3_2023_preBPix --category-name base --modules-file run3_2023_modulesrdf --keep-and-drop-file run3_keep_and_drop_file  --workers 50 --branch 0 --dataset-name Tau_2023Cv1

echo ""
echo "##############################################################################################################################################################################"
echo "MC 2023 postBPix"
echo ""

law run PreprocessRDF --version picoTest --config-name run3_2023_postBPix --category-name base --modules-file run3_2023_modulesrdf --keep-and-drop-file run3_keep_and_drop_file  --workers 50 --branch 0 --dataset-name GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00

echo ""
echo "##############################################################################################################################################################################"
echo "DATA 2023 postBPix"
echo ""

law run PreprocessRDF --version picoTest --config-name run3_2023_postBPix --category-name base --modules-file run3_2023_modulesrdf --keep-and-drop-file run3_keep_and_drop_file  --workers 50 --branch 0 --dataset-name Tau_2023Dv1

